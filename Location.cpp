/*
 * Location.cpp -- implementation of a Location class
 *
 * a Location identifies a point on the Earth (or any sphere)
 * Internally, a Location contains a longitude and latitude
 *
 * The methods are:
 *
 *      nullary constructor (latitude == longitude == 0.0)
 *	constructor(longitude, latitude)
 *	set_location(latitude, longitude)
 *	distance_to(otherLocation)
 *	print()
 *      to_string()
 */

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <math.h>
#include <boost/math/constants/constants.hpp>

#include "Location.h"

using namespace std;

Location::Location()
{
	longitude = 0.0;
	latitude  = 0.0;
}

Location::Location(double someLatitude, double someLongitude)
{
	latitude  = someLatitude;
	longitude = someLongitude;
}
void Location::set_location(double someLatitude, double someLongitude)
{
	longitude = someLongitude;
	latitude  = someLatitude;
}

string Location::to_string()
{
        stringstream result;

        result << setprecision(2) << fixed
               << latitude << ' ' << longitude;

        return result.str();
}

void Location::print()
{
	printf("%.2f %.2f", latitude, longitude);
}
	

/*
 * from 
 *  http://www.codeproject.com/Articles/22488/
 *          Distance-using-Longitiude-and-latitude-using-c
 */

// distance function using a pointer
//  arg: a pointer to a location
//  ret: the distance between the calling location and the arg
//
double Location::distance_to(Location *locp)
{
	return distance_to(*locp);
}

// distance function using a reference param
//  arg: a reference to a Location
//  ret: the distance between the calling location and the arg
//
double Location::distance_to(Location &x)
{
	double	longx, latx, dist;
        latx  = x.latitude;
	longx = x.longitude;

        double lat_1_rad, lon_1_rad, lat_2_rad, lon_2_rad;
	lat_1_rad = latitude * (boost::math::constants::pi<double>() / 180);
	lon_1_rad = longitude * (boost::math::constants::pi<double>() / 180);
	lat_2_rad = latx * (boost::math::constants::pi<double>() / 180);
	lon_2_rad = longx * (boost::math::constants::pi<double>() / 180);

        double delta_lat, delta_lon;
	delta_lat = lat_1_rad - lat_2_rad;
	delta_lon = lon_1_rad - lon_2_rad;

	// Calculate sin^2 (delta / 2) for both lat and long
	double sdlat = pow(sin(delta_lat / 2), 2);
	double sdlon = pow(sin(delta_lon / 2), 2);

	// Radius of the Earth (approximate)
	const double radius_earth_miles = 3963;
	
	// http://en.wikipedia/org/wiki/Haversine_formula
	// d=2r*asin(sqrt(sin^2((lat1-lat2)/2)+cos(l1)cos(l2)sin^2((lon2-lon1)/2)))
	//  if t = sqrt(sin^2((lat1-lat2)/2)+cos(l1)cos(l2)sin^2((lon2-lon1)/2)
	//  -> d = 2 * radius_earth * asin (t)	
	double t = sqrt(sdlat + (cos(lat_1_rad) * cos(lat_2_rad) * sdlon));
	dist = 2 * radius_earth_miles * asin(t);
        
    return dist;
}