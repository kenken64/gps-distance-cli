/*
*
*  COMP 15 PROJ 2 - RT 
*
*  findpath.cpp 
*  findpath class implementation: This file implements a series of functions on 
* the findpath class.  
*
*  Modified By (UTLN): dkalra01
*           On       : 3/04/2020
*
*/

#include <iostream> 
#include <string> 
#include <fstream> 
#include "findpath.h"
using namespace std; 

//Default Constructor 
FindPath::FindPath()
{ 
    roadtrip.initialize_graph(1000); 
}

//Function populate graph 
//Parameters: string filename  
//Returns: void 
//Does: populates the graph with city locations and their respective latitude 
// and longitude values in degrees and minutes. 
void FindPath::populate_graph(string filename)
{
    
    ifstream infile;
    infile.open(filename);
    if (!infile.is_open()) {
        cerr << "Unable to open " << filename << endl; 
        exit(1);
    }
    
    while(!infile.eof())
    {
        string name; 
        int lat_deg; 
        int lat_min; 
        int lon_deg; 
        int lon_min; 
        
        infile >> name;     
        
        infile >> lat_deg; 
        infile >> lat_min; 
        double lat_dd = lat_deg + lat_min/60; 
            
        infile >> lon_deg; 
        infile >> lon_min; 
        double lon_dd = lon_deg + lon_min/60; 
        
        Location city(lat_dd, lon_dd); 
        roadtrip.add_vertex(city);         
    }
    
}



