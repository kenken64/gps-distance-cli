/*
*
*  COMP 15 PROJ 2 - RT 
*
*  findpath.h 
*  findpath class: This file includes the class definition.  
*
*  Modified By (UTLN): dkalra01
*           On       : 3/04/2020
*
*/

#include <iostream> 
#include <string> 
#include "Graph.h"
#include "Location.h"

using namespace std; 

#ifndef _FINDPATH_H_
#define _FINDPATH_H_

class FindPath 
{ 
public: 
    //Default constructor 
    FindPath(); 
    
    //Class functions 
    void populate_graph(string filename); 
    
private: 
    Graph<Location> roadtrip; 
    
}; 

#endif 
