CXX      = /usr/bin/clang++
CXXFLAGS = --std=c++11 -Wshadow -Wall -Wextra -g3

%.o: %.cpp $(shell echo *.h)
	$(CXX) $(CXXFLAGS) -c $<

findpath: main.o Location.o Graph.o 
	$(CXX) -o $@ $^

clean: 
	rm -f rt findpath *.o

# Add some rules for rt website
include Makefile.publish

