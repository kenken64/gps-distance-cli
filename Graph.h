#include <stdlib.h>
#include <time.h>       // rand seed
#include <string>
#include <vector>       // vector of tokenized input
#include <queue>        // breadth-first search
#include <iostream>
#include <fstream>      // file()
#include <map>
using namespace std;

class Graph
{
    private:
        typedef map<string, int*> list;         
        typedef map<string, list> matrix;          
        matrix adjTable;
        vector<string> tokenize(string input, string delims);                
        vector<string> getAdjacent(string label);
        void updateEdge(string vertex1, string vertex2, int status);

    public:
        Graph(int size=0);
        void addVertex(string label);
        void removeVertex(string vertex);
        void addEdge(string vertex1, string vertex2);
        void removeEdge(string vertex1, string vertex2);
        void breadthFirst(string start, string end, int maxpaths);
        void printAdjTable();
        int size();
};
