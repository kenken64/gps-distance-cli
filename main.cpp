#include <iostream>
#include <string>
#include <bits/stdc++.h>
#include <sstream>
#include "Location.h"
#include "Graph.h"
#include <fstream>
#include <unistd.h>
#include <vector>

using namespace std;

/**
 * this c++ function read data file into vector.
 */
void readDataFile(string vdatFilename, vector<string> &vecOfvdat)
{
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        //    printf("Current working dir: %s\n", cwd);
    }
    else
    {
        perror("getcwd() error");
        exit(0);
    }
    string s(cwd);
    ifstream fin(s + '/' + vdatFilename);
    int size = 0;
    string strvalue;
    while (getline(fin, strvalue))
    {
        if (strvalue.size() > 0){
            vecOfvdat.push_back(strvalue);
        }
        size = size + 1;
    }
    fin.close();
}

double str2doub(string str)
{
    double d;
    stringstream iss(str);
    iss >> d;
    return d;
}

int getLongLat(vector<string> &vecOfvdat, vector<string> &vecCol, double &longtitude, double &latitude, string val)
{
    for (vector<string>::size_type k = 0; k != vecOfvdat.size(); k++)
    {
        string tokenVal = vecOfvdat[k].substr(0, vecOfvdat[k].find("	"));
        transform(tokenVal.begin(), tokenVal.end(), tokenVal.begin(), ::toupper);
        if (tokenVal == val)
        {
            string longlatstr = vecOfvdat[k].substr(tokenVal.length() + 1, vecOfvdat[k].find('\n'));

            size_t pos = 0;
            size_t newpos;
            while (pos < longlatstr.size())
            {
                newpos = longlatstr.find_first_of(" \t\n", pos);
                if (pos == 2)
                {
                    newpos = newpos + 1;
                }
                string subvv = longlatstr.substr(pos, newpos);
                vecCol.push_back(subvv);
                if (pos < longlatstr.size())
                {
                    pos = pos + subvv.length();
                }
            }
            break;
        }
    }

    if (vecCol.size() > 0)
    {
        for (vector<string>::size_type x = 0; x != vecCol.size(); x++)
        {
            if (x == 0)
            {
                longtitude = str2doub(vecCol[x]);
            }

            if (x == 1)
            {
                longtitude = longtitude + (str2doub(vecCol[x]) / 60);
            }

            if (x == 2)
            {
                latitude = str2doub(vecCol[x]);
            }

            if (x == 3)
            {
                latitude = latitude + (str2doub(vecCol[x]) / 60);
            }
        }
        return 0;
    }
    else
    {
        cout << "Unable to locate city" << endl;
        return 1;
    }
}

void handlePath(vector<string> &vecOfadat,
                vector<string> &vecOfvdat,
                Graph *g, string fromCityCommand, string toCityCommand, string maxpaths)
{
    for (vector<string>::size_type k = 0; k != vecOfvdat.size(); k++)
    {
        string tokenVal = vecOfvdat[k].substr(0, vecOfvdat[k].find("	"));
        //cout << "xxx" << tokenVal << endl;
        g->addVertex(tokenVal);
    }
    for (vector<string>::size_type i = 0; i != vecOfadat.size(); i++)
    {
        auto fromCityVal = vecOfadat[i].substr(0, vecOfadat[i].find("	"));
        auto toCityVal = vecOfadat[i].substr(fromCityVal.length() + 1, vecOfadat[i].find("\n"));
        bool isCityFound = false;
        for (vector<string>::size_type l = 0; l != vecOfvdat.size(); l++)
        {
            auto citySearch = vecOfvdat[l].substr(0, vecOfvdat[l].find("	"));
            if (citySearch == fromCityVal)
            {
                fromCityVal = citySearch;
                isCityFound = true;
                break;
            }
            else
            {
                isCityFound = false;
            }
        }
        for (vector<string>::size_type m = 0; m != vecOfvdat.size(); m++)
        {
            auto citySearch = vecOfvdat[m].substr(0, vecOfvdat[m].find("	"));
            if (citySearch == toCityVal)
            {
                toCityVal = citySearch;
                isCityFound = true;
                break;
            }
            else
            {
                isCityFound = false;
            }
        }
        if (isCityFound == true)
            g->addEdge(fromCityVal, toCityVal);
    }
    int maxpathInt= 0;
    stringstream yy(maxpaths); 
    yy >> maxpathInt;
    if(maxpathInt > 0){
        g->breadthFirst(fromCityCommand, toCityCommand, maxpathInt);
    }else{
        cout << "Max paths must be greater than 0" << endl;
    }
}

/**
 * this is the main program.
 */
int main(int argc, char **argv)
{
    vector<string> vecOfvdat;
    vector<string> vecOfadat;

    if (argc < 3)
    {
        std::cerr << "Usage: findpath vertex_data edge_data" << endl;
        return 1;
    }
    readDataFile(argv[1], vecOfvdat);
    Graph *g = new Graph(vecOfvdat.size());
    readDataFile(argv[2], vecOfadat);
    while (1)
    {
        string mystr;
        string mystrPath;

        string arr[4];
        getline(cin, mystr, '\n');
        mystrPath = mystr;
        transform(mystr.begin(), mystr.end(), mystr.begin(), ::toupper);
        string delimiter = " ";
        string token = mystr.substr(0, mystr.find(delimiter)); // token is "exit list dist"

        if (token == "EXIT")
        {
            exit(0);
        }
        else if (token == "LIST")
        {
            for (vector<string>::size_type i = 0; i != vecOfvdat.size(); i++)
            {
                auto tokenVal = vecOfvdat[i].substr(0, vecOfvdat[i].find("	"));
                cout << tokenVal << endl;
            }
            cout << "-EOT-" << endl;
        }
        else if (token == "DIST")
        {
            int i = 0;
            stringstream ssin(mystr);
            while (ssin.good() && i < 4)
            {
                ssin >> arr[i];
                ++i;
            }
            double fromLong;
            double fromLat;
            double toLong;
            double toLat;
            vector<string> fromLonglatVect;
            vector<string> toLonglatVect;
            int isOk = 0;
            isOk = getLongLat(vecOfvdat, fromLonglatVect, fromLong, fromLat, arr[1]);
            if (isOk == 0)
            {
                isOk = getLongLat(vecOfvdat, toLonglatVect, toLong, toLat, arr[2]);
                if (isOk == 0)
                {
                    Location froml;
                    Location tol;
                    froml.set_location(fromLong, fromLat);
                    tol.set_location(toLong, toLat);
                    double distance = froml.distance_to(tol);
                    if (distance > 0)
                    {
                        cout << distance << endl;
                    }
                }
            }

            cout << "-EOT-" << endl;
        }
        else if (token == "PATH")
        {
            int i = 0;
            stringstream ssin(mystrPath);
            while (ssin.good() && i < 4)
            {
                ssin >> arr[i];
                ++i;
            }
            handlePath(vecOfadat, vecOfvdat, g, arr[1], arr[2], arr[3]);
        }
        else
        {
            cerr << "Invalid command" << endl;
        }
    }
}