## GPS Distance Path finder

Load the path finder program

```
./findpath ta.vdat ta2.adat
list
dist MA-Boston OR-Portland
path MA-Boston OR-Portland 1
quit

```

Check the distance of the one location to another

```
DIST MA-Boston NJ-Newark
925.507
-EOT-
```

Check the shortest path from one place to another 

```
path MA-Boston NJ-Newark 2
PATH 1 :: 0 MA-Boston 42.35 71.08 39
RI-Providence 41.83 71.40 86 CT-NewHaven
41.32 72.92 77 NJ-Newark 40.73 74.17
PATH 2 :: 0 MA-Boston 42.35 71.08 39
RI-Providence 41.83 71.40 63 MA-Springfield
42.10 72.57 57 CT-NewHaven 41.32 72.92 77
NJ-Newark 40.73 74.17
-EOT
```
