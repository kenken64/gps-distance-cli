#include <stdlib.h>
#include <time.h>       // rand seed
#include <string>
#include <vector>       // vector of tokenized input
#include <queue>        // breadth-first search
#include <iostream>
#include <fstream>      // file()
#include <map>
#include "Graph.h"

using namespace std;

#define LABEL first      // we use a map to store vertex LABEL:VALUE, so for convenience I prefer to access them via vertex.LABEL & row.COLS instead of vertex.first and row.second
#define VALUE second
#define COLS second
#define CONTAINS(obj, val) (obj.find(val) != obj.end())  // helper func because c++ has no 'in' keword, e.g. `val in obj`

const string alphabet[36] = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};    // for generating random graph vertex labels

const string RED = "\033[1;31m";
const string YELLOW = "\033[1;33m";
const string ENDCOLOR = "\033[0m";

vector<string> Graph::tokenize(string input, string delims=",; -") {
    vector<string> tokens;
    size_t prev = 0, pos;
    // find next occurence of next delimiter after prev delimiter, then make a token using everything between those two positions
    while ((pos = input.find_first_of(delims, prev)) != string::npos) {
        if (pos > prev)
            tokens.push_back(input.substr(prev, pos-prev));
        //tokens.push_back(input.substr(pos, 1));             // this adds the delimiter as a token as well, useful for catching operators as tokens (e.g. +-()*/^)
        prev = pos+1;
    }
    // catch trailing token after the last delimiter
    if (prev < input.length())
        tokens.push_back(input.substr(prev, string::npos));

    return tokens;
}

 // get all verticies adjacent to a given vertex
vector<string> Graph::getAdjacent(string label) {
    vector<string> adj;
    // iterate through that vertex's row in adjTable
    for (auto& vertex: adjTable[label]) {
        // push any vertecies (that arent the current one) where edge = 1
        if (*(vertex.VALUE) == 1 && vertex.LABEL != label)
            adj.push_back(vertex.LABEL);
    }
    return adj;
}

void Graph::updateEdge(string vertex1, string vertex2, int status) {
    // check to make sure both verticies exist
    if (!CONTAINS(adjTable, vertex1)) {
        cout << endl << RED << "[X] Vertex with label '" << vertex1 << "' does not exist." << ENDCOLOR << endl;
        return;
    }
    if (!CONTAINS(adjTable, vertex2)) {
        cout << endl << RED << "[X] Vertex with label '" << vertex2 << "' does not exist." << ENDCOLOR << endl;
        return;
    }
    // the adjTable is automatically consistent (A-B and B-A)
    *(adjTable[vertex1][vertex2]) = status;
}

Graph::Graph(int size) {
    // init a default graph with up to 36 verticies labeled A through Z
    for (int i=0; i < size && i < 37; i++)
        addVertex(alphabet[i]);
}

void Graph::addVertex(string label) {
    if (CONTAINS(adjTable, label)) {
        //cout << endl << RED << "[X] Vertex with label '" << label << "' already exists." << ENDCOLOR << endl;
        return;
    }
    if (label == "" || label == " ") return;
    
    list new_adj_row;
    new_adj_row[label] = new int(0);                    // vertexes are always disconnected to themselves
    
    for (auto& vertex: adjTable)                        // seed the new adjacency table row with edges to all the other vertecies
        new_adj_row[vertex.LABEL] = new int(0);         // new vertex starts disconnected to all the others
    
    adjTable[label] = new_adj_row;                      // add the new row to the adjTable
    
    for (auto& row: adjTable)                           // add a new column to all the other rows
        row.COLS[label] = new_adj_row[row.LABEL];       // re-use the pointers created above, so that both point to the same int
}

void Graph::removeVertex(string vertex) {
    if (!CONTAINS(adjTable, vertex)) {
        cout << endl << RED << "[X] Vertex with label '" << vertex << "' does not exist." << ENDCOLOR << endl;
        return;
    }
    adjTable.erase(vertex);                             // remove the vertex's row
    for (auto& row: adjTable)                           // remove the column from all the other rows
        row.COLS.erase(vertex);
}

void Graph::addEdge(string vertex1, string vertex2) {
    updateEdge(vertex1, vertex2, 1);
}

void Graph::removeEdge(string vertex1, string vertex2) {
    updateEdge(vertex1, vertex2, 0);
}

void Graph::breadthFirst(string start, string end, int maxpaths) {
    if (!CONTAINS(adjTable, start)) {
        cout << endl << RED << "[X] Vertex with label '" << start << "' does not exist." << ENDCOLOR << endl;
        return;
    }
    if (!CONTAINS(adjTable, end)) {
        cout << endl << RED << "[X] Vertex with label '" << end << "' does not exist." << ENDCOLOR << endl;
        return;
    }
    if (start == end) {
        cout << YELLOW << "[√] Start and End are the same vertex! " << end << "-" << start << "" << ENDCOLOR << endl;
        return;
    }
    if (getAdjacent(start).empty() || getAdjacent(end).empty()) {
        cout << RED << "[X] Start or End is orphaned. No path exists." << ENDCOLOR << endl;
        return;
    }

    queue<string> q;
    map<string, string> tofrom;
    tofrom[start] = start;          // set the beginning of the tofrom beadcrumb trail to the start vertex
    q.push(start);
    string root;
    int counter = 0;
    int foundHowMany = 0;
    while (q.size() > 0) {
        root = q.front(); 
        q.pop();
        for (string& next: getAdjacent(root)) {
            if (!CONTAINS(tofrom, next)) {
                tofrom[next] = root;
                q.push(next);
                counter++;
            }
            if (next == end) {
                if(counter >= maxpaths){
                    cout << endl << YELLOW << "[√] Found path! " << end;
                    string last = end;
                    while (tofrom[last] != start) {
                        last = tofrom[last];
                        cout << "-" << last;
                    }
                    cout << "-" << start << ENDCOLOR << endl << endl;
                    foundHowMany++;
                }
                counter = 0;
            }
        }
    }
    if(foundHowMany == 0)
        cout << RED << "[X] Path not found." << ENDCOLOR << endl;
}

void Graph::printAdjTable() {
    cout << endl << " ";
    // print top labels
    for (auto& row: adjTable)
        cout << "|" << row.LABEL;
    cout << endl;
    // print each row
    for (auto& row: adjTable) {
        cout << row.LABEL << "|";
        for (auto& col: row.COLS)
            cout << (*(col.VALUE) ? RED : YELLOW) << *(col.VALUE) << " " << ENDCOLOR;
        cout << endl;
    }
    cout << endl;
}

int Graph::size() {
    return adjTable.size();
}